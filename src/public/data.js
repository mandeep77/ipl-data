const noOfMatchPath = `https://ipl-data7777.herokuapp.com/database/mathesPerYear`
const noOfWonPath = `https://ipl-data7777.herokuapp.com/database/matchesWon`
const totalRunConceded = `https://ipl-data7777.herokuapp.com/database/extra`
const economyPath = `https://ipl-data7777.herokuapp.com/database/economy`


//fetch for number of matches played per year.
fetch(noOfMatchPath)
    .then(r => r.json())
    .then(mainOfNoOfMatches);

//fetch for matchesWon        
fetch(economyPath)
    .then(r => r.json())
    .then(mainEconomyPath);


//fetch for extra runs conceded        
fetch(totalRunConceded)
    .then(r => r.json())
    .then(mainTotalRunConceded);

//fetch for extra runs conceded        
fetch(noOfWonPath)
    .then(r => r.json())
    .then(won);


//for setting colours for highcharts
Highcharts.setOptions({
    chart: {
        backgroundColor: {
            linearGradient: [0, 0, 500, 500],
            stops: [
                [0, 'rgb(255, 255, 255)'],
                [1, 'rgb(240, 240, 255)']
            ]
        },
        borderWidth: 2,
        plotBackgroundColor: 'rgba(255, 255, 255, .9)',
        plotShadow: true,
        plotBorderWidth: 1
    }
});





//function for number of matches played per year.
function mainOfNoOfMatches(data) {
    const year = [];
    for (let d of data) {
        year.push([d['season'], d['noOfMatches']]);
    }
    console.log(year)



    Highcharts.chart('matchPlayedPerYear', {


        chart: {
            type: 'column'
        },
        title: {
            text: 'NUMBER OF MATCHES PLAYED PER SEASON.'
        },
        subtitle: {
            text: 'IPL DATA.'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'number of matches'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Matches Played: <b>{point.y} </b>'
        },
        series: [{
            name: 'Number of Matches',
            data: year,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: 'Matches Played:{point.y}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}
//function for extra runs conceded
function mainTotalRunConceded(data) {
    const name = [];
    for (let i of data) {
        name.push([i['batting_team'], i['extra']]);
    }
    console.log(name)



    Highcharts.chart('extraRuns', {


        chart: {
            type: 'column'
        },
        title: {
            text: 'EXTRA RUNS CONCEDED.'
        },
        subtitle: {
            text: 'IPL DATA.'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Total Extra Runs Conceded'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Extra Runs Conceded: <b>{point.y} </b>'
        },
        series: [{
            name: 'YEAR',
            data: name,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: 'Runs:{point.y}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}

//function for top 10 economy bowlers
function mainEconomyPath(data) {
    const player = [];
    for (let i of data) {
        player.push([i['bowler'], i['rate']]);
    }
    console.log(player)



    Highcharts.chart('eco', {


        chart: {
            type: 'column'
        },
        title: {
            text: 'Economy of Top 10 Players in the year 2015.'
        },
        subtitle: {
            text: 'IPL DATA.'
        },
        xAxis: {
            type: 'category',
            labels: {
                rotation: -45,
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Economy'
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: 'Economy: <b>{point.y:.2f} </b>'
        },
        series: [{
            name: 'Number of Matches',
            data: player,
            dataLabels: {
                enabled: true,
                rotation: -90,
                color: '#FFFFFF',
                align: 'right',
                format: '{point.y:.2f}', // one decimal
                y: 10, // 10 pixels down from the top
                style: {
                    fontSize: '13px',
                    fontFamily: 'Verdana, sans-serif'
                }
            }
        }]
    });
}

//function for number of matches won by each team.

function format(data) {
    let result = {}, counter = -1;
    for (let year in data) {
        counter++;
        for (let name in data[year]) {
            if (!result.hasOwnProperty(name)) {
                result[name] = [];
                for (let J = 0; J < counter; J++) {
                    result[name].push(0)
                }
            }
            result[name].push(data[year][name])
        }
        for (name in result) {
            result[name].length < counter + 1 && result[name].push(0)
        }
    }

    return result;
}
function won(data) {

    let data1 = {};
    data.forEach(obj => {
        let season = obj.season;
        if(data1[season]) {
            let winner = obj.winner;
            let noOfTimes = obj.noOfTimes;
            data1[season][winner] = noOfTimes;
        }else {
            let winner = obj.winner;
            let noOfTimes = obj.noOfTimes;
            data1[season] = {};
            data1[season][winner] = noOfTimes;
        }
    })

    let formattedData = format(data1);
    let years = Object.keys(data);
    let finalData = [];
    for (let data in formattedData) {
        let obj = {
            name: data,
            data: formattedData[data]
        };
        finalData.push(obj);
    }

    Highcharts.chart('won', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'NUMBER OF MATCHES WON BY EACH TEAM'
        },
        xAxis: {
            categories: years
        },
        yAxis: {
            min: 0,
            title: {
                text: 'years'
            }
        },
        legend: {
            reversed: true
        },
        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },
        series: finalData

    });
}