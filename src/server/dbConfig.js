let mysql = require('mysql');
const dotenv = require('dotenv')
dotenv.config();


let connection = mysql.createPool({
  host: process.env.PD_DB_HOST,
  user: process.env.PD_DB_USER,
  password: process.env.PD_DB_PASS,
  database: process.env.PD_DB_DATABASE,
  connectionLimit: 10
});



 
module.exports = connection; 