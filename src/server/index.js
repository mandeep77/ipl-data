const csvtojson = require('csvtojson');
const fs = require('fs');
const path = require('path');
const base = path.join(__dirname, '../');
const matches = base + './data/matches.csv';
const outputPath = base + './public/output/';
const deliveries = base + './data/deliveries.csv'
const { matchesPlayedPerYear, matchesWonByTeam, ExtraRunsConceded, top10Economical,v } = require('./ipl');
const express = require('express');
const { request, response } = require('express');
console.log(matches)
let app = express();

function main() {
    csvtojson()
        .fromFile(matches) //fetching from matches.csv
        .then((matches) => {
            const matchesPlayedYear = matchesPlayedPerYear(matches);
            const wonByTeam = matchesWonByTeam(matches);
            csvtojson()
                .fromFile(deliveries) //fetching from deliveries.csv
                .then((deliveries) => {
                    let extraRuns = ExtraRunsConceded(matches, deliveries, 2016);
                    let eco = top10Economical(matches, deliveries, 2015);
                    let virat = v(matches,deliveries,2016)
                    console.log(virat)
                    saveFilesTtoJSON(matchesPlayedYear, wonByTeam, extraRuns, eco);   // sending functions return value to save to convert to json files

                })
        })
}

//function to save Data in Json files
/**
 * save functions saves the data to the json output files
 * 
 * @param {object} noMatches  This is the return value of the number of matches won each year.
 * @param {object} matchesWon This is the return value of number of matches won by each team.
 * @param {object} extra This is the return value of the extra runs conceded by each team.
 * @param {object} eco This is the return value of the top 10 Economical bowlers
 */

function saveFilesTtoJSON(noMatches, matchesWon, extra, eco) {
    noMatches = JSON.stringify(noMatches);
    matchesWon = JSON.stringify(matchesWon);
    extra = JSON.stringify(extra);
    eco = JSON.stringify(eco);

    

    fs.writeFile(outputPath + 'noMatches.json', noMatches, 'utf8', (err) => { 
        if (err) {
            console.log(err);
        }
    })
    fs.writeFile(outputPath + 'matchesWon.json', matchesWon, 'utf8', (err) => {
        if (err) {
            console.log(err);
        }
    })
    fs.writeFile(outputPath + 'extra.json', extra, 'utf8', (err) => {
        if (err) {
            console.log(err);
        }
    })
    fs.writeFile(outputPath + 'eco.json', eco, 'utf8', (err) => {
        if (err) {
            console.log(err);
        }
    })


}


main();
