
// function to return number of matches per season.

/**
 * This function counts Number of matches held per year(season).
 * 
 * @param {object[]} matches This is the matches data provided to this function.
 * @returns {object}  
 */

const matchesPlayedPerYear = (matches) => {

    let matchesCount = {}; //  obj that will store season and number of matches.
    matches.forEach(match => {

        let year = match.season;
        if (matchesCount.hasOwnProperty(year)) { //checking if matchesCount has that season already or not.
            matchesCount[year]++;                //if it has we will increment it.
        }
        else matchesCount[year] = 1;             //if it does not have we ll add that season i.e we ll add tht property and init with 1
    })
    return matchesCount;
}




//function to return number of matches won my each team per season.

/**
 * This functions count number of matches won by each team
 * 
 * @param {object[]} matches This is the matches data provided to this function.
 * @returns {object} returns number of matches per year.
 */

const matchesWonByTeam = (matches) => {
    let winningTeam = {}
    matches.forEach(match => {
        let year = match.season;   // contains th year
        let winner = match.winner; // contains the winning team of the above year.

        if (winningTeam.hasOwnProperty(year)) {
            if (winningTeam[year].hasOwnProperty(winner)) { // checking is winner property is defined or not.
                winningTeam[year][winner] += 1;
            }
            else {
                winningTeam[year][winner] = 1;
            }
        }
        else {
            winningTeam[year] = {};      //this is the object in which winning teams count will me addded and this ll be assigned to year property as a value
            winningTeam[year][winner] = 1;
        }

    })
    return  winningTeam ;
}

// function to find ExtraRunsConceded

/**
 * This function calculates Extra runs conceded by each team.
 * 
 * @param {object[]} matches  takes matches data
 * @param {object[]} deliveries  takes deliveries data
 * @param {Number} year  takes year 
 * 
 * @returns {object} returns extra runs conceded by each team as a object
 */

const ExtraRunsConceded = (matches, deliveries, year) => {
    let teams = {};

    matches.filter(match => match.season == year)
        .forEach(match => {
            deliveries.filter(delivery => delivery.match_id == match.id)
                .forEach(delivery => {
                    if (teams[delivery.batting_team]) {
                        teams[delivery.batting_team] += Number(delivery.extra_runs);
                    }
                    else {
                        teams[delivery.batting_team] = Number(delivery.extra_runs);

                    }

                })

        })
    return teams;
}


// function to find top 10 Economical bowlers.

/**
 * This function calculates top 10 economical bowlers.
 * 
 * @param {object[]} matches  takes matches data   
 * @param {object[]} deliveries takes deliveries data
 * @param {Number} year takes year
 * 
 * @returns {object} returns top 10 economical bowlers as object.
 */

function top10Economical(matches, deliveries, year) {
    let bowlerData = {};

    matches.filter(match => match.season == year).forEach(match => {
        deliveries.filter(delivery => delivery.match_id == match.id).forEach(
            delivery => {

                if (bowlerData[delivery.bowler]) {
                    bowlerData[delivery.bowler].ballsDelivered += 1;
                    bowlerData[delivery.bowler].runsGiven += Number(delivery.total_runs)

                }
                else {
                    bowlerData[delivery.bowler] = {};
                    bowlerData[delivery.bowler].runsGiven = Number(delivery.total_runs)
                    bowlerData[delivery.bowler].ballsDelivered = 1;
                }

            }
        )
    })
    let NameWithEconomy = [];
    let bowlersEconomy = {};
    Object.keys(bowlerData).forEach(bowler => {

        let overs = (bowlerData[bowler].ballsDelivered) / 6;
        let runs = bowlerData[bowler].runsGiven;
        let rate = runs / overs;

        NameWithEconomy.push([bowler, rate]);

    })

    NameWithEconomy.sort((a, b) => a[1] - b[1]);

    NameWithEconomy.slice(0, 10).forEach(player => {

        bowlersEconomy[player[0]] = player[1];

    })

    return bowlersEconomy;


}

let v = function (matches, deliveries, year) {
    let result ={}
    matches.filter(match=>match.season == year)
    .forEach(match=>{
        deliveries.filter(delivery=>match.id ==delivery.match_id)
        .forEach(delivery=>{
            if(delivery.batsman == 'V Kohli'){
                if(result[delivery.batsman]){
                    result[delivery.batsman]+= Number(delivery.total_runs)
                }else{
                    result[delivery.batsman] = Number(delivery.total_runs)

                }
            }
        })
    })
    return result;
}



module.exports = { matchesPlayedPerYear, matchesWonByTeam, ExtraRunsConceded, top10Economical,v };