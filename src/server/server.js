const express = require('express');
const path = require('path')
const getDataFromDatabase = require('./sqlQuery');
const base = path.join(__dirname, '../');
const port = process.env.PORT || 5000;


let app = express()

app.use(express.static(path.join(base + 'public')))

//app.use('/eco,json',express.static())


app.get('/database/:sqlFileName', (req, res) =>{
    let sqlFileName = req.params.sqlFileName;
    getDataFromDatabase(sqlFileName)
        .then(response => res.status(200).send(response))
        .catch(err => res.status(500).send(err))

})

app.listen(port, () => console.log("listening to " + port))


