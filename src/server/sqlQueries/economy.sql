
SELECT 
    (totalRuns / overs) as rate,
    bowler
FROM (
    SELECT 
        SUM(total_runs) as totalRuns,
        bowler,
        (COUNT(ball) / 6) as overs
    FROM MATCHES
    JOIN DELIVERIES 
    ON MATCHES.id = DELIVERIES.match_id
    WHERE season = 2015
    GROUP BY bowler
) tab
ORDER BY rate
LIMIT 10
