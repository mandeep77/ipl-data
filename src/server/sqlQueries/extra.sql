SELECT 
	SUM(extra_runs) as extra,
    batting_team 
from MATCHES 
JOIN DELIVERIES 
ON MATCHES.id = DELIVERIES.match_id
WHERE SEASON = 2016
GROUP BY batting_team;