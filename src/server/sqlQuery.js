const con = require('./dbConfig');
const fs = require('fs');
const path = require('path');

function getDataFromDatabase(sqlFileName) {
    return new Promise((resolve, reject) => {
        fs.readFile(path.resolve(__dirname, `./sqlQueries/${sqlFileName}.sql`), 'utf-8', (err, sql) => {
            if(err) {
                reject(err);
            }else {
                con.getConnection((err, conn) => {
                    if(err) {
                        reject(err);
                        conn.release();
                    }else {
                        conn.query(sql, (err, result, field) => {
                            if(err) {
                                reject(err);
                                conn.release();
                            }else {
                                resolve(result);
                                conn.release();
                            }
                        })
                    }
                })
            }
        })
    })
}

module.exports = getDataFromDatabase;